package praktikum8;

import praktikum6.Meetodid;

public class Kuulujutugeneraator {

	public static void main(String[] args) {
		
		String[] meheNimed = {"Rando", "Markus","Kurelus" };
		String[] naisenimed = {"Randoliina", "Randiina","Randmiina","Randoh","Randa"};
		String[] tegusõnad = {"vaatavad koos Serbia filmi","teevad ebasündsaid tegevusi põõsas","parandavad..torusid..","jooksid koos oma kohustuste eest","otsustasid koos ära põgeneda"};

		String mees = suvalineElement(meheNimed);
		String naine = suvalineElement(naisenimed);
		String tegu = suvalineElement(tegusõnad);

		System.out.println(mees + " ja " + naine + " " + tegu);

	}

	private static String suvalineElement(String[] nimed){
		
		return nimed[Meetodid.suvalineArv(0, nimed.length - 1)];
	}


}
