package praktikum8;

import lib.TextIO; // Programm on lõpetamata ja vigane

public class TagurpidiArvud {

	public static void main(String[] args) {

		int[] arvud = new int[10];
		for (int i = 0; i < arvud.length; i++) {
			System.out.println("Sisesta oma " + i + ". arv");
			arvud[i] = TextIO.getInt();

			
		}
		tagurpidi(arvud);
	}

	public static int[] tagurpidi(int[] oigetpidi) {
		int[] tagurpidi = new int[oigetpidi.length];
		for (int i = oigetpidi.length; i > 0; i--) {
			int indeks = oigetpidi.length - 1 - i;
			tagurpidi[i] = oigetpidi[indeks];
		}
		return tagurpidi;

	}

}
