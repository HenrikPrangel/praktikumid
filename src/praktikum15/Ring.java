package praktikum15;

public class Ring {
	Punkt keskpunkt;
	int raadius;

	public Ring(int i, Punkt p1) {
		keskpunkt = p1;
		raadius = i;
	}

	public double ymberm66t() {
		return 2 * Math.PI * raadius;

	}
	
	public double pindala(){
		return Math.PI * Math.pow(raadius, 2);
		
	}
}
