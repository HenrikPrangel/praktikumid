package praktikum15;

public class Joon {

	Punkt alguspunkt;
	Punkt l6ppunkt;

	public Joon(Punkt p1, Punkt p2) {
		alguspunkt = p1;
		l6ppunkt = p2;

	}

	@Override
	public String toString() {
		return "Joon(" + alguspunkt + ", " + l6ppunkt + ")";
	}

	public double joonePikkus() {
		return Math.sqrt(((alguspunkt.x -l6ppunkt.x)*(alguspunkt.x -l6ppunkt.x)) + ((alguspunkt.y-l6ppunkt.y)*(alguspunkt.y-l6ppunkt.y)));
	}
}
