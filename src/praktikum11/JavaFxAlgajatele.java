package praktikum11;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.stage.Stage;

public class JavaFxAlgajatele extends Application {

	@Override
	public void start(Stage arg0) throws Exception {
		Pane pane = new Pane();
//		pane.setStyle("-fx-background-color: #FFFFFF;");
		Rectangle kast2 = new Rectangle(100, 60, 190, 40);
		kast2.setFill(Color.BLUE);
		Rectangle kast = new Rectangle(100, 100, 190, 40);
		Rectangle kast3 = new Rectangle(100, 140, 190, 40);
		kast3.setFill(Color.YELLOW);
		pane.getChildren().add(kast);
		pane.getChildren().add(kast2);
		pane.getChildren().add(kast3);

		Scene stseen = new Scene(pane, 600, 400);
		arg0.setScene(stseen);
		arg0.show();

	}

}
