package praktikum11;

public class Positiivnetaisarv {

	public static void main(String[] args) {

		System.out.println(astenda(2, 5));

	}

	// private static int astenda(int i, int j) {
	// int c = i;
	// for (int p = 0; p < j-1; p++) {
	// c *= i;
	//
	// }
	// return c;
	// }

	private static int astenda(int i, int j) { // rekrusiivselt
		if (j <= 1)
			return i;
		return i * astenda(i, j - 1);

	}

}
