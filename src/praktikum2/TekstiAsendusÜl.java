package praktikum2;

import lib.TextIO;

public class TekstiAsendusÜl {

	public static void main(String[] args) {

		String text;

		System.out.println("Tervist!");
		System.out.print("Palun sisesta mingisugune tekst ");
		text = TextIO.getlnString();
		System.out.println("Sinu tekst on siin: ");
		System.out.print(text.replace("a", "_"));

	}

}
