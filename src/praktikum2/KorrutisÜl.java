package praktikum2;

import lib.TextIO;

public class KorrutisÜl {

	public static void main(String[] args) {

		int arv1; // Esimene arv korrutamiseks
		int arv2; // Teine arv korrutamiseks
		int arv3; // Kahe eelneva arvu korrutis

		System.out.print("Tervist!");
		System.out.print("Palun sisesta esimene arv");
		arv1 = TextIO.getInt();
		System.out.print("Palun sisesta teine arv");
		arv2 = TextIO.getInt();

		arv3 = arv1 * arv2;
		System.out.print("Vastus on ");
		System.out.println(arv3);
	}

}
