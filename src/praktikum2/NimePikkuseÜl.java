package praktikum2;

import lib.TextIO;

public class NimePikkuseÜl {

	public static void main(String[] args) {

		// ctrl + 7 = tekitab koodist kommentaari ja vastupidi

		String nimi;

		System.out.print("Tervist!");
		System.out.print("Palun sisesta oma nimi.");
		nimi = TextIO.getlnString();
		System.out.print("Sinu nime pikkus on " + nimi.length() + " tähte.");

	}

}
