package praktikum2;

import lib.TextIO;

public class GrupiÜl {

	public static void main(String[] args) {

		int arv1; // Inimeste arv
		int arv2; // Nõutav grupi suurus
		int arv3; // Saadav gruppide arv ja ka väljajäänud inimeste arv

		System.out.print("Tervist!");
		System.out.print("Kui palju on inimesi?");
		arv1 = TextIO.getInt();
		System.out.print("Kui suur peaks olema üks grupp");
		arv2 = TextIO.getInt();

		arv3 = arv1 / arv2;
		System.out.print("Moodustada saab " + arv3 + " gruppi");
		arv3 = arv1 % arv2;
		System.out.print("ja üle jääb " + arv3 + " inimest");

	}

}
