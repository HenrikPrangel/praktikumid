package praktikum13;

public class UlMassiividega {

	public static void main(String[] args) {
		int[] massiiv = { 2, 4, 5, 6, 3, 5 };
		// tryki(massiiv);
		int[][] maatriks = { { 1, 1, 1, 1, 1 }, { 2, 3, 4, 5, 6 }, { 3, 4, 5, 6, 7 }, { 4, 5, 6, 7, 8 },
				{ 5, 6, 7, 8, 9 }, };
		tryki(maatriks);
		tryki(ridadeSummad(maatriks));
		System.out.println(korvalDiagonaaliSumma(maatriks));

	}

	public static void tryki(int[] massiiv) {
		for (int i = 0; i < massiiv.length; i++) {
			System.out.print(massiiv[i] + " ");
		}
		System.out.println();
	}

	// public static void tryki(int[][] maatriks) {
	// for (int i = 0; i < maatriks.length; i++) {
	// for (int j = 0; j < maatriks.length; j++) {
	// System.out.print(maatriks[i][j] + " ");
	// }
	// System.out.println();
	// }
	// }

	public static void tryki(int[][] maatriks) { // Two-Dimensional Matrix
													// method beginning
		for (int[] rida : maatriks) {
			tryki(rida);
		}
	}// End

	public static int[] ridadeSummad(int[][] maatriks) { // Beginning
		int[] summad = new int[maatriks.length];
		for (int i = 0; i < maatriks.length; i++) {
			summad[i] = massiivSumma(maatriks[i]);

		}
		return summad;
	} // End

	// Sum of all the elements of a one-dimensional array
	private static int massiivSumma(int[] massiiv) { // Beginning
		int sum = 0;
		for (int i = 0; i < massiiv.length; i++) {
			sum += massiiv[i];
		}
		return sum;
	} // End
	
	public static int korvalDiagonaaliSumma(int[][] maatriks) { // Sum of Diagonal Beginning
		int sum = 0;
		for (int i = 0; i < maatriks.length; i++) {
			for (int j = 0; j < maatriks.length; j++) {
				if (i + j == maatriks.length - 1){
					sum += maatriks[i][j];
				}
			}
		}
		return sum;
		
	}//End

}
