package praktikum10;

import lib.TextIO;

public class SuurimElement{

	public static void main(String[] args) {

		System.out.println("Tervist, palun sisesta, kui palju arve tahad oma massiivi");
		int t = TextIO.getInt();

		System.out.println(max(sisestus(t)));

	}

	private static int max(int[] massiiv) {

		int rmax = 0;
		for (int i = 0; i < massiiv.length; i++) {
			int max = massiiv[i];
			if (rmax < max) {
				rmax = max;
			}

		}
		return rmax;
	}

	public static int[] sisestus(int t) {

		int[] massiiv = new int[t];
		for (int i = 0; i < massiiv.length; i++) {
			int x = i + 1;
			System.out.println("Sisesta " + x + "." + " arv");
			massiiv[i] = TextIO.getInt();

		}
		return massiiv;
	}
}
