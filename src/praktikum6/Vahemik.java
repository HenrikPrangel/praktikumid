
package praktikum6;

import lib.TextIO;

public class Vahemik { // class algus

	static int k;
	static int g;

	public static void main(String[] args) { // main meetod aalgus
		int h = 0;
		System.out.println("Tervist, palun sisesta kaks arvu!");
		while (h <= 0) {

			k = TextIO.getInt();
			g = TextIO.getInt();
			boolean q = vahemik(k, g);
			if (q == true) {
				System.out.println("Well done, õigesti arvasid!");
				h++;
			} else {
				System.out.println("Proovi uuesti");
			}

		}

	} // main meetod lõpp

	public static boolean vahemik(int x, int y) { // teine meetod algus

		if (x >= 20 && y <= 40) {
			return true;

		} else {
			return false;

		}

	} // teine meetod lõpp

} // class lõpp
