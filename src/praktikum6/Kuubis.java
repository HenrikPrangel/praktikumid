package praktikum6;

import lib.TextIO;

public class Kuubis { // class algus

	static double k;

	public static void main(String[] args) { // main meetod aalgus
		System.out.println("Tervist, palun sisesta arv!");
		k = TextIO.getDouble();

		System.out.println(kuubis(k));

	} // main meetod lõpp

	public static double kuubis(double x) { // teine meetod algus
		double f = x * x * x;
		return f;

	} // teine meetod lõpp

} // class lõpp
