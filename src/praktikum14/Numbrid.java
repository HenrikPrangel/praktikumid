package praktikum14;

import java.util.ArrayList;

public class Numbrid {
	public static void main(String[] args) {
		
		ArrayList<String> failiRead = new ArrayList<String>();
		System.out.println(TekstifailRead.Faililugeja("numbrid.txt"));
		System.out.println(failiRead);
	}
	
	public static ArrayList<Double> teeNumbriteks(ArrayList<String> read) {
		ArrayList<Double> numbrid = new ArrayList<Double>();
		for (String rida : read) {
			double nr = Double.parseDouble(rida);
			numbrid.add(nr);
		}
		return numbrid;
	}
}
