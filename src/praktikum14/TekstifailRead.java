package praktikum14;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Collections;

public class TekstifailRead {

	public static void main(String[] args) {

		System.out.println(Faililugeja("read.txt"));

	}

	public static ArrayList<String> Faililugeja(String failinimi) {

		// punkt tähistab jooksvat kataloogi
		String kataloogitee = TekstifailRead.class.getResource(".").getPath();

		// otsime samast kataloogist kala.txt-nimelist faili
		File file = new File(kataloogitee + failinimi);

		ArrayList<String> loetudread = new ArrayList<String>();
		try {
			// avame faili lugemise jaoks
			BufferedReader in = new BufferedReader(new FileReader(file));
			String rida;

			// loeme failist rida haaval
			while ((rida = in.readLine()) != null) {
				loetudread.add(rida);

			}
		} catch (FileNotFoundException e) {
			System.out.println("Faili ei leitud: \n" + e.getMessage());
		} catch (Exception e) {
			System.out.println("Error, jee, mingi muu error: " + e.getMessage());
		}
//		Collections.sort(loetudread);
		return loetudread;
	}

}
