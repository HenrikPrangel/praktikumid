package praktikum12;

import java.applet.Applet;
import java.awt.*;

public class Yleminek extends Applet {

	public void paint(Graphics g) {
		int x = getWidth();
		int y = getHeight();
		for (int i = 0; i < y; i++) {
			double concentrate = (double) i/y;
			int juice =  (int) (concentrate * 255)  ;
			Color color = new Color(juice, juice, juice);
			g.setColor(color);
			g.drawLine(0, i, x, i);

		}

	}
}
