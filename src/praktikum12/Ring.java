package praktikum12;
import java.applet.Applet;
import java.awt.Color;
import java.awt.Graphics;


// Pole Veel valmis

@SuppressWarnings("serial")
public class Ring extends Applet {

    private Graphics g;
    
    public void paint(Graphics g) {
        this.g = g;
        drawBackraound();
        drawspiral();
    }
    
    /**
     * Katab tausta valgega
     */
    public void drawBackraound() {
        int w = getWidth();
        int h = getHeight();
        g.setColor(Color.white);
        g.fillRect(0, 0, w, h);
    }
    
    /**
     * Draws a spiral
     */
    
    public void drawspiral() {
    	for (int i = 0; i < 20; i++) {
    		g.setColor(Color.black);
    		int centerX = getWidth() / 2;
    		int centerY = getHeight() / 2;
    		int radius =+ 1;
    		
    		for (double corner = 0; corner <= Math.PI * 4; corner = corner + .03) {
    			int x = (int) (radius * Math.cos(corner));
    			int y = (int) (radius * Math.sin(corner));
    			g.fillRect(centerX + x, centerY + y, 2, 2);
			
		}
        }
    }
}