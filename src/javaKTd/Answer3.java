package javaKTd;

public class Answer3 {

	public static void main(String[] args) {
		System.out.println(result(new double[] { 0., 1., 2., 3., 4. }));
		System.out.println(result(new double[] { 1., 2., 3. }));
		System.out.println(result(new double[] { 0., 2., 2., 5., 1. }));
		System.out.println(result(new double[] { 7., 9., 8., 6., 10. }));
	}

	public static double result(double[] marks) {
		double e = 0;
		double c = 0;
		double d = 11;
		for (int i = 0; i < marks.length; i++) {
			if (marks[i] > c) {
				c = marks[i];
			}
			if (marks[i] < d) {
				d = marks[i];
			}
			e += marks[i];
		}
		double f = (e - c - d) / (marks.length-2);
		return f;
	}
}
